require_relative '../spec_parser'

describe SpecParser do
  it 'handles basic GET request' do
    result = SpecParser.parse('spec/scenarios/get.yml')[0]
    request = result[:request]
    response = result[:response]

    expect(result[:name]).to eq('Basic GET Request')
    expect(request[:url]).to eq('http://localhost:3000/foo')
    expect(request[:method]).to eq('GET')
    expect(request[:headers]).to eq([{"Content-Type"=>"application/json"}, {"Accept-Language"=>"en-US"}])
    expect(response[:body].to_json).to eq([{id: 1, title:"My first entry", content:"This is definitely the first entry.", created_at:"2017-01-27T21:03:17.974Z", updated_at:"2017-01-27T21:03:17.974Z" }, { id:2, title:"My second entry", content:"This is definitely the second entry.", created_at:"2017-01-27T21:03:42.141Z", updated_at:"2017-01-27T21:03:42.141Z" }].to_json)
    expect(response[:code]).to eq("200")
  end

  it 'handles basic GET request with no headers' do
    result = SpecParser.parse('spec/scenarios/get.yml')[1]
    request = result[:request]
    response = result[:response]

    expect(request[:headers]).to eq({})
  end

  it 'handles a body of a POST request' do
    result = SpecParser.parse('spec/scenarios/post.yml')[0]
    request = result[:request]

    expect(request[:body].to_json).to eq({title: "the title", content: "the content"}.to_json)
    expect(request[:method]).to eq('POST')
    expect(request[:headers]).to eq([{"Content-Type"=>"application/json"}, {"Accept-Language"=>"en-US"}])
  end
end
