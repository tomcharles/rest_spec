require 'net/http'
require 'json'
require_relative './spec_parser'
require_relative './spec_finder'

def do_get(test)
  do_request (test) do |path|
    Net::HTTP::Get.new(path)
  end
end

def do_post(test)
  do_request_with_body (test) do |path|
    Net::HTTP::Post.new(path)
  end
end

def do_put(test)
  do_request_with_body (test) do |path|
    Net::HTTP::Put.new(path)
  end
end

def do_delete(test)
  do_request (test) do |path|
    Net::HTTP::Delete.new(path)
  end
end

def do_request_with_body(test)
  do_request (test) do |path|
    request = yield (path)
    request.body = test[:request][:body].to_json
    request
  end
end

def do_request(test)
  uri = URI.parse(test[:request][:url])
  http = Net::HTTP.new(uri.host, uri.port)

  request = yield uri.path
  headers = test[:request][:headers]
  headers.each do |it|
    request.initialize_http_header(it)
  end

  http.request(request)
end

def body_matcher (body)
  if body == 'null'
    Regexp.new(/.*/)
  else
    Regexp.new(Regexp.quote(body).gsub("\\{\\{any\\}\\}", '.*'))
  end
end

directory=ENV['DIRECTORY']
root_url=ENV['ROOT_URL']

SpecFinder.findSpec(directory).each do |spec|
  describe spec do
    SpecParser::parse(spec).each do |test|
      it "#{test[:name]}" do
        expected = body_matcher(test[:response][:body].to_json)

        test[:request][:url] = "#{root_url}#{test[:request][:url]}" unless root_url.empty? 
        actual = case method = test[:request][:method]
        when 'GET'
          do_get test 
        when 'PUT'
          do_put test
        when 'POST'
          do_post test
        when 'DELETE'
          do_delete test
        else
          raise "invalid HTTP method: #{method}"
        end

        expect(actual.code).to eq(test[:response][:code])
        expect(actual.body).to match(expected)
      end
    end
  end
end
