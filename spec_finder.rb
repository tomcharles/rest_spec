class SpecFinder

  def self.findSpec(dir)
    Dir.glob("#{dir}/**/*.yml")
  end

end
