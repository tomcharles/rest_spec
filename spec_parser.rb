require 'yaml'
require 'json'

class SpecParser
  def self.parse(filename)
    File
      .open(filename).read.split('---')
      .map do |spec|
        YAML.load(spec)
      end
      .map do |spec|
        {
          name: spec['name'],
          request: {
            url: spec['request']['url'],
            method: spec['request']['method'],
            headers: spec['request']['headers'] || {},
            body: spec['request']['body']
          },
          response: {
            body: spec['response']['body'],
            code: spec['response']['code'].to_s
          }
        }
      end
  end
end
